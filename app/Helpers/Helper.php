<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\User;

class Helper
{
    public static function user_name($userID){
        $user = User::findOrFail($userID);
        return $user->name;
    }
}