<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    

    public function blankOption()
    {
        $option = new Option();
        $option->answer = 0;
    	return $option;
    }

    public function options()
    {
    	return $this->hasMany('App\Option');
    }

    public function quizzes()
    {
    	return $this->belongsToMany('App\Quiz');
    }


    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public static function getQuestion($id)
    {
        $question = Question::findOrFail($id);
        return $question;
    }

    public static function points($id){
        $question = Question::findOrFail($id);
        return $question->points;
    }
}
