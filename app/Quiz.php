<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    public function questions()
    {
    	return $this->belongsToMany('App\Question');
    }


    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function quiz_sections()
    {
    	return $this->hasMany('App\QuizSection');
    }

}
