<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\QuizFormRequest;
use Illuminate\Support\Facades\DB;
use App\Quiz;
use App\Question;
use Illuminate\Http\Request; 
use Auth;
use App\User;
use App\QuizPrivacy;
use App\SectionQuestion;

class QuizController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$user = Auth::user();
		$quizzes = Quiz::where('user_id', $user->id)->orderBy('id', 'desc')->paginate(10);


		return view('quizzes.index', compact('quizzes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$quiz = New Quiz();

		return view('quizzes.create', compact('quiz'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(QuizFormRequest $request)
	{

		$quiz = new Quiz();
        $user = Auth::user();
		$quiz->name = $request->input("name");
		$quiz->schedule = $request->input("schedule");
        $quiz->user_id = $user->id;
		$quiz->save();

		return redirect()->route('quizzes.index')->with('message', 'Item created successfully.');
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$quiz = Quiz::findOrFail($id);

		return view('quizzes.show', compact('quiz'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$quiz = Quiz::findOrFail($id);
		$user = Auth::user();
		$sharedTo = QuizPrivacy::where('quiz_id', '=', $id)->get();
		$users = User::whereNotIn('email', $sharedTo->pluck('user_email'))->get();
		$sections = $quiz->quiz_sections()->get();
		$section_questions = [];
		foreach($sections as $section){
			foreach($section->questions()->get() as $question){
				$section_questions[] = $question->question_id;
			}
		}
		$questions = Question::where('user_id', $user->id)->whereNotIn('id', $quiz->questions()->pluck('questions.id'))->whereNotIn('id', $section_questions)->orderBy('id', 'desc')->paginate(5);
		return view('quizzes.edit', compact('quiz', 'questions', 'users', 'sharedTo', 'sections'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(QuizFormRequest $request, $id)
	{
		$users = $request->input("visibility");

		$quiz = Quiz::findOrFail($id);
		$quiz->name = $request->input("name");
		$quiz->schedule = $request->input("schedule");
		$quiz->quiz_type = $request->input("quiz_type");
		$quiz->save();
		if(!empty($users)){
			foreach ($users as $user) {
				$quiz_privacy = new QuizPrivacy();
				$quiz_privacy->quiz_id = $id;
				$quiz_privacy->user_email = $user;
				$quiz_privacy->save();
			}
		}
		return redirect()->route('quizzes.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$quiz = Quiz::findOrFail($id);
		$quiz->delete();

		return redirect()->route('quizzes.index')->with('message', 'Item deleted successfully.');
	}

	/**
	 * Adds question to a quiz.
	 * 
	 * @param int $id
	 * @param int $question_id
	*/
	public function addQuestion($id, $question)
	{
		$quiz = Quiz::findOrFail($id);
		$quiz->questions()->attach($question);

		

		return redirect()->route('quizzes.edit', compact('quiz'));	
	}

	public function addSection(Request $request){
		DB::table('quiz_sections')->insert([
			'quiz_id'      => $request->quiz_id,
			'name'          => $request->section_name
		]);
		return redirect()->back();
	}

	/**
	 * Removes a question from a quiz.
	 * 
	 * @param int $id
	 * @param int $question_id
	*/
	public function removeQuestion($id, $question)
	{
		$quiz = Quiz::findOrFail($id);
		$quiz->questions()->detach($question);	

		return redirect()->route('quizzes.edit', compact('quiz'));	
	}

	public function saveSectionQuestions(Request $request)
	{
		$sectionID = $request->input('section_id');
		$questions = $request->input('questions');
		foreach ($questions as $question) {
			$section_question = new SectionQuestion();
			$section_question->question_id = $question;
			$section_question->quiz_section_id = str_replace("section", "", $sectionID);
			$saved = $section_question->save();

			if(!$saved){
			    App::abort(500, 'Error');
			}
		}
		// return "". $equal;
	}
}
	