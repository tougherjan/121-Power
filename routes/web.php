<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'QuizController@index');
Route::get('/addsection', 'QuizController@addSection')->name('quizzes.addSection');
Route::post('/savesectionquestions', 'QuizController@saveSectionQuestions')->name('quizzes.savesectionquestions');
//Route::get('/', 'QuizController@index');

Route::resource('quizzes','QuizController');
Route::get('/quizzes/{id}/addquestion/{question}', 'QuizController@addQuestion')->name('quizzes.addquestion');
Route::get('/quizzes/{id}/removequestion/{question}', 'QuizController@removeQuestion')->name('quizzes.removequestion');

Route::resource('questions', 'QuestionController');
Route::resource('questions.options','OptionController', ['except' => ['index']]);
#Route::resource("quizzes.questions","QuestionController", ['except' => ['index']]);
#Route::get('/questions', 'QuestionController@index')->name('questions.index');
Route::post('/createAnswer','AnswerQuizController@createAnswer')->name('user.answers');
Route::post('/createAnswers/{quiz_id}', 'AnswerQuizController@createAnswers')->name('user.createAnswers');
Route::post('/checkManually/{quizID}/{questionID}','AnswerQuizController@checkManualy')->name('answerquizzes.checkManually');
Route::get('/live_quiz/{id}/{round}/{question_id}/mark/{answer}/{user}/{question_number}', 'AnswerQuizController@mark_answer')->name('mark_answer');
Route::resource('answerquizzes', 'AnswerQuizController');
#Route::delete('/options/{id}/delete', 'OptionController@destroy')->name('options.destroy');
Auth::routes();

Route::get('/home', 'QuizController@index');
Route::post('/storeAnswer', 'AnswerQuizController@storeAnswer')->name('answers.store');
Route::get('/updateQuestion', 'AnswerQuizController@updateQuestion')->name('quizzes.trial');
Route::get('/answerquizzes/administer/{id}', 'AnswerQuizController@administerQuiz')->name('quizzes.administer');
Route::get('/{quizid}/nextquestion', 'AnswerQuizController@getNext')->name('quizzes.nextquestion');
Route::get('/showanswer/{quizID}/{questionID}', 'AnswerQuizController@showanswer')->name('quizzes.showanswer');
Route::get('/nextround/{quizID}', 'AnswerQuizController@nextRound')->name('quizzes.nextround');
Route::get('/welcome/{id}', 'AnswerQuizController@welcome')->name('answerquizzes.welcome');
Route::get('/endquiz/{id}', 'AnswerQuizController@endQuiz')->name('answerquizzes.end_quiz');
Route::get('/correct_answer/{quizID}/{questionID}', 'AnswerQuizController@showCorrectAnswer')->name('quizzes.showCorrectAnswer');
Route::post('/updateTime', 'AnswerQuizController@updateTime')->name('quizzes.updateTime');