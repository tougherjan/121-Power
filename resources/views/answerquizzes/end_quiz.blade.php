@extends('layout')
@section('header')

 <link type="text/css" href="/css/an.css" rel="stylesheet">

<center><h2 class="fnt note rounded" style="width: 23%">FINAL RESULTS</h2><center>

	<br>
    <div class="row">
    	<div class="col-md-12 tbl-header">
	 		<table class="tbl" cellpadding="0" cellspacing="0" border="0">
	 			<thead>
					<tr>
						<th>Name of Participant</th>
						<th style="text-align: center">Easy</th>
						<th style="text-align: center">Average</th>
						<th style="text-align: center">Difficult</th>
						<th style="text-align: center">Total Points</th>
					</tr>
				</thead>
			</table>
		</div>

		 <div class="tbl-content">
            <table class="tbl" cellpadding="0" cellspacing="0" border="0">
            	<tbody>
				@foreach($pairs as $pair)
					<tr>
						<td>&nbsp;&nbsp;&nbsp;{!! $pair['username'] !!}</td>
						<td style="text-align: center">{!! $pair['easy'] !!}</td>
						<td style="text-align: center">{!! $pair['medium'] !!}</td>
						<td style="text-align: center">{!! $pair['difficult'] !!}</td>
						<td style="text-align: center">{!! $pair['points'] !!}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div><br>

    <a class="bb" href="{{route('answerquizzes.end_quiz', $id)}}">End Quiz</a>
	@endsection
