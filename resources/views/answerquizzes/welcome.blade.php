<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>CS QUIZ App</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.css" rel="stylesheet"> -->
    <!-- <link type="text/css" href="/css/new.css" rel="stylesheet"> -->
    <link type="text/css" href="/css/welcome.css" rel="stylesheet">
    <!-- <link type="text/css" href="{!! asset('css/app.css') !!}" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" type="text/css" href="{!! asset('css/quiz.css') !!}"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <!-- <link href="starter-template.css" rel="stylesheet"> -->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('css')
</head>

<body class="logo">
    @if($user==$quiz_owner)
    <a class="breezy" href="{{ route('quizzes.administer', $id) }}">START QUIZ</a>
    @else
    <form method="POST" action="{{ route('answers.store') }}">
         <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input name="id" type = "hidden" value={{$id}}>
        <button class="breezy" >START QUIZ</button>
      </form>
    @endif
</body>
</html>
