<div class="row">
   <div class="row">
        <div class="col-md-12 tbl-header">
            @if(count($pairs))
            <table class="tbl" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr>
                        <th>Name of School</th>
                        <th style="text-align: center">Answer</th>
                        <th style="text-align: center">Result</th>
                        <th style="text-align: center">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        
        <div class="tbl-content">
            <table class="tbl" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                    @foreach($pairs as $pair)
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;{!! $pair['username'] !!}</td>
                        <td style="text-align: center">{!! $pair['answer'] !!}</td>
                        <td style="text-align: center"> {!! $pair['points'] !!} </td>
                        <td style="text-align: center"> <a href="{{ route('mark_answer', ['quiz_id' => $quiz_id, 'round' => $round, 'question_id' => $question_id, 'check' => 5, 'user' => $pair['user_id'], 'question_number' => $question_number]) }}" class="btn btn-xs btn-success" >
                        <i class="glyphicon glyphicon-ok"></i> Check</a>
                        <a href="{{ route('mark_answer', ['quiz_id' => $quiz_id, 'round' => $round, 'question_id' => $question_id, 'check' => 0, 'user' => $pair['user_id'], 'question_number' => $question_number]) }}" class="btn btn-xs btn-danger" >
                        <i class="glyphicon glyphicon-remove"></i> Wrong </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
            <h3 class="text-center alert alert-info">Empty!</h3>
        @endif
    </div>
</div>

    