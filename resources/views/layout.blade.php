<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <script type="text/x-mathjax-config">
  MathJax.Hub.Config({
    tex2jax: {
      inlineMath: [ ['$','$'], ["\\(","\\)"] ],
      processEscapes: true
    }
  });
</script>

<script type="text/javascript"
    src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>CS QUIZ App</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.css" rel="stylesheet">
    <!-- <link type="text/css" href="/css/new.css" rel="stylesheet"> -->
    <link type="text/css" href="/css/an.css" rel="stylesheet">
    <!-- <link type="text/css" href="{!! asset('css/app.css') !!}" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="{!! asset('css/quiz.css') !!}">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <!-- Custom styles for this template -->
    <!-- <link href="starter-template.css" rel="stylesheet"> -->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('css')
</head>

<body>
    <nav class="navbar-color navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="fnt navbar-brand" style="color: #fff" href="/">CS QUIZ APP</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="fnt nav navbar-nav navbar-right" >
                    @if (Auth::guest())
                        <li class="underline-on-hover"><a style="color: #fff" href="{{ url('/login') }}">Login</a></li>
                        <li class="underline-on-hover"><a style="color: #fff" href="{{ url('/register') }}">Register</a></li>
                    @else

                        <li class="underline-on-hover"><a style="color: #fff" href="{{ route('quizzes.index')}}">Quizzes</a></li>
                        <li class="underline-on-hover"><a style="color: #fff" href="{{ route('questions.index')}}">Questions</a></li>
                        <li class="underline-on-hover"><a style="color: #fff" href="{{ route('answerquizzes.index')}}">Answer Quizzes</a></li>

                        <li class="underline-on-hover" class="dropdown">
                            <a style="color: #fff; background-color: transparent" href="#" class="dropdown-toggle name" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    <div class="container">
        @yield('header')
        @yield('content')
    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{!! asset('js/jquery-1.11.2.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
    
    
    @yield('scripts')

</body>
</html>
