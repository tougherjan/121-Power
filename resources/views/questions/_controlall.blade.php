<a class="btn btn-xs btn-primary" href="{{ route('questions.show', ['question'=>$question->id]) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
<a class="btn btn-xs btn-warning" href="{{ route('questions.edit', ['question'=>$question->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
<form action="{{ route('questions.destroy', ['question'=>$question->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
</form>