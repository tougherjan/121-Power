<a class="btn btn-xs btn-success" href="{{ route('quizzes.addquestion', ['id'=>$quiz->id, 'question'=>$question->id]) }}"><i class="glyphicon glyphicon-ok"></i> Add to Quiz</a>
<a class="btn btn-xs btn-primary" href="{{ route('questions.show', ['question'=>$question->id]) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
<a class="btn btn-xs btn-warning" href="{{ route('questions.edit', ['question'=>$question->id]) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
