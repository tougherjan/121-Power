<style>
    .modal-dialog {
        position: absolute;
        top: 50px;
        right: 100px;
        bottom: 0;
        left: 100px;
        z-index: 10040;
        overflow: auto;
        overflow-y: auto;
    }
    .modal-content {
        position: relative;
        max-height: 400px;
        max-width: 700px;
        padding: 15px;
    }
</style>
<div id="myModal" class="modal">
  <div class='modal-dialog'>
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h2>Modal Header</h2>
      <form action="{{ route('quizzes.addSection') }}" method="GET">
        <input type="hidden" name="quiz_id" value="{{$quiz->id}}">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label>Name of Section</label>
       <!--  <input type="text" name=""> -->
       <input type="text" name="section_name" style="color: black;">
        <button>Add</button>
      </form>
    </div>
    <div class="modal-body">
  </div>
 </div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
</div>