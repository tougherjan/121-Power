@extends('layout')

@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <link type="text/css" href="css/new.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Quizzes / Create </h1>
    </div>
@endsection

@section('content')
    <!--@include('error')-->

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('quizzes.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @include('quizzes._quiz')

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('quizzes.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.input-group.date').datepicker({
      format: "yyyy-mm-dd",
      todayBtn: true,
      autoclose: true
    });
  </script>
@endsection